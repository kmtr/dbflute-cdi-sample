<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Employees</h1>
        <form action="${pageContext.servletContext.contextPath}/" method="POST">
            <input type="text" name="name" />
            <input type="submit" value="add" />
        </form>
        <table border="1">
            <c:forEach var="employee" items="${employees}">
                <tr>
                    <td><c:out value="${employee.id}" /></td>
                    <td><c:out value="${employee.name}" /></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
