package com.example.dbflutecdi.db.cbean.nss;

import com.example.dbflutecdi.db.cbean.cq.EmployeeCQ;

/**
 * The nest select set-upper of EMPLOYEE.
 * @author DBFlute(AutoGenerator)
 */
public class EmployeeNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected EmployeeCQ _query;
    public EmployeeNss(EmployeeCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
