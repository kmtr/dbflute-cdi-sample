package com.example.dbflutecdi.db.cbean;

import com.example.dbflutecdi.db.cbean.bs.BsEmployeeCB;

/**
 * The condition-bean of EMPLOYEE.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class EmployeeCB extends BsEmployeeCB {
}
