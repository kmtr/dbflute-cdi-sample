package com.example.dbflutecdi.db.exentity;

import com.example.dbflutecdi.db.bsentity.BsEmployee;

/**
 * The entity of EMPLOYEE.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Employee extends BsEmployee {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
