package com.example.dbflutecdi.db.exbhv;

import com.example.dbflutecdi.db.allcommon.DBFluteGenerate;
import com.example.dbflutecdi.db.bsbhv.BsEmployeeBhv;

/**
 * The behavior of EMPLOYEE.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@DBFluteGenerate
public class EmployeeBhv extends BsEmployeeBhv {
}
