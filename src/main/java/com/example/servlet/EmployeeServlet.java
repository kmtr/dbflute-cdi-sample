/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.servlet;

import com.example.dbflutecdi.db.cbean.EmployeeCB;
import com.example.dbflutecdi.db.exbhv.EmployeeBhv;
import com.example.dbflutecdi.db.exentity.Employee;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kameturu
 */
@WebServlet("/")
public class EmployeeServlet extends HttpServlet {

    @Inject
    EmployeeBhv employeeBhv;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EmployeeCB employeeCB = new EmployeeCB();
        List<Employee> employees = employeeBhv.selectList(employeeCB);
        request.setAttribute("employees", employees);
        getServletContext()
                .getRequestDispatcher("/WEB-INF/jsp/index.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        
        Employee employee = new Employee();
        employee.setName(name);
        employeeBhv.insert(employee);
        
        response.sendRedirect(getServletContext().getContextPath());
    }
}
